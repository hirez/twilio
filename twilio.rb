#!/usr/bin/env ruby

require 'rubygems'
require 'bundler/setup'

require 'sinatra'
require 'yaml'
require 'twilio-ruby'


enable :sessions
set :session_secret, 'Felle helle felle helle felle hola. Chris Waddle.'

$config = YAML.load_file("twilio.yaml")

def set_forward(fuser)
  @phonelist = $config['forwards']
  @forward = @phonelist[fuser]

  File.open('target', 'w') {|f| f.write(@forward.to_yaml) }
end

get '/' do
  @phonelist = $config['forwards']
  erb :index
end

post '/select' do
  @user = params[:user] if params[:user]
  @phonelist = $config['forwards']

  if @phonelist[@user]
    set_forward(@user)
    @tnumber = @phonelist[@user]
    erb :forwarding
  else
    "Oh? Really?"
  end
end

get '/forward/:user' do
  @user = params[:user] if params[:user]
  @phonelist = $config['forwards']
  if @phonelist[@user]
    set_forward(@user)
    @tnumber = @phonelist[@user]
    erb :forwarding
  end
end

get '/show' do
  @tnumber = YAML.load_file('target')
  erb :forwarding
end

get '/twiml' do
  Twilio::TwiML::Response.new do |r|
    r.Gather :numDigits => '1', :action => '/gather1', :method => 'get' do |g|
      g.Say 'Welcome to amalgamated industries.', :voice => 'alice'
      g.Pause
      g.Say 'For a brief impression that your actions may be meaningful, press 1.', :voice => 'alice'
      g.Say 'For Extreme Noise Terror in session, press 2.', :voice => 'alice'
      g.Say 'To accept delivery of the complete internet cat picture archive, press 3.', :voice => 'alice'
      g.Pause
      g.Say 'I am not even supposed to be here today.'
      g.Pause
      g.Say 'Go and make the tea then. One sugar, please.', :voice => 'alice'
      g.Pause
      g.Say 'Biscuit?'
      g.Pause
      g.Say 'Oh go on then.', :voice => 'alice'
    end
  end.text
end

get '/gather1' do
  redirect '/twiml' if params['Digits'] == '4'
  Twilio::TwiML::Response.new do |r|
    case params['Digits']
    when '1'
      r.Say 'I am afraid that nothing happened.', :voice => 'alice'
    when '2'
      r.Say 'John Peel is revered as a saint among my people.', :voice => 'alice'
    when '3'
      r.Say 'Is is about ethics in games journalism.'
    end
  end.text
end

