require 'rubygems'
require 'sinatra'

set :env,  :production
disable :run

require './twilio.rb'

run Sinatra::Application
